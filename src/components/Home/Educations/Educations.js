import React from "react";
import Skill from "./Education/Education";
const Skills = () => {
  return (
    <>
      <Skill
        description="Software Engineering studies TWIN specialty (technology web and internet). Learned software architecture and full-stack development."
        startDate="2016"
        endDate="NOW"
        title="Software engineering student"
        company="ESPRIT"
        link="http://esprit.tn"
      />
      <Skill
        description="Rank 250 in the National Entry contest for the engineering studies cycles (PT
          Section)."
        startDate="2014"
        endDate="2016"
        title="Preparatory cycle for engineering studies"
        company="IPEIEM"
        link="http://www.ipeiem.rnu.tn/"
      />
    </>
  );
};

export default Skills;
